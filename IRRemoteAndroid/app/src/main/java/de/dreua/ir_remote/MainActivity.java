package de.dreua.ir_remote;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {

    public static final String TAG = "IR_REMOTE";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
    }

    private void sendIR(final int code) {
        final long start = System.currentTimeMillis();
        textView.setText("?Sending " + code + " ...");
        Log.d(TAG, "in sendIR after " + (System.currentTimeMillis() - start));
        Thread t = new Thread() {
            @Override
            public void run() {
                Log.d(TAG, "in run after " + (System.currentTimeMillis() - start));
                String exception = null;
                super.run();
                try {
                    URL u = new URL("http://192.168.0.74/ir?code=" + code + "&noresponse=1");

                    Log.d(TAG, "in URL created after " + (System.currentTimeMillis() - start));
                    HttpURLConnection c = (HttpURLConnection) u.openConnection();
                    c.setConnectTimeout(100);
                    c.setReadTimeout(100);
                    c.getResponseCode();
                    c.disconnect();

                    Log.d(TAG, "got content after " + (System.currentTimeMillis() - start));
                } catch (MalformedURLException e) {
                    exception = e.getLocalizedMessage();
                } catch (IOException e) {
                    exception = e.getLocalizedMessage();
                }
                final String status = exception == null ? ("OK :) [Sent: " + code + "]") : exception;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(status);
                    }
                });
            }
        };
        t.start();
        Thread.yield();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            sendIR(50680);
            return true;
        } else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            sendIR(50552);
            return true;
        } else
            return super.onKeyDown(keyCode, event);
    }

    public void onAux(View v) {
        sendIR(50556);
    }

    public void onOnOff(View v) {
        sendIR(50664);
    }
}
